<?php

// Read -------------
$result = \App\Models\UserUser::read(false, ['orderBy'=>'name']);

print_r($result['body']);

// Create -------------
$result = \App\Models\UserUser::create([
                        'name'=>'John',
                        'status'=>'true'
                    ]);
print_r($result);

// Update -------------
$id = 1;
$result = \App\Models\UserUser::update($id, [
                        'name'=>'John A.'
                    ]);
print_r($result);
