<?php
namespace App\Models;

use angelrove\ApiClient\ApiRestCrud\ApiRestCrud;

class User extends ApiRestCrud
{
    protected const CONF = array(
        'ENTITY' => '/v1/user',
        'ENTITY_READ' => '/v2/public/users',
        'ENTITY_READ_ID' => '',
        'UPDATE_METHOD' => 'PATCH',
    );

    protected static $columns = array(
        "name",
        "status",
    );

    public static function onError($message)
    {
        echo $message;
    }
}
