<?php
/**
 * ApiRestModelInterface
 */

namespace angelrove\ApiClient\ApiRestCrud;

interface ApiRestModelInterface
{
    public static function onError($response);
}
