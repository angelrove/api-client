<?php
/**
 * ApiRestCrud
 */

namespace angelrove\ApiClient\ApiRestCrud;

use angelrove\ApiClient\CallApi;

class ApiRestCrudHelper
{
    protected static $api_enviroment;
    protected static $api_auth_token;
    protected static $api_accept_language;

    public static function __initConf($api_enviroment, $api_auth_token, $api_accept_language)
    {
        self::$api_enviroment = $api_enviroment;
        self::$api_auth_token = $api_auth_token;
        self::$api_accept_language = $api_accept_language;
    }

    //--------------------------------------------------------------
    // PUBLIC
    //--------------------------------------------------------------
    public static function help_create(array $conf, $columns, $data)
    {
        $body = self::parseData($data, $columns);
        return self::callApi('POST', $conf['ENTITY'], array(), $body);
    }

    public static function help_update(array $conf, $columns, $id, $data)
    {
        $body = self::parseData($data, $columns);
        $entity = $conf['ENTITY'].'/'.$id;

        return self::callApi($conf['UPDATE_METHOD'], $entity, array(), $body);
    }

    public static function help_delete(array $conf, $id)
    {
        $entity = $conf['ENTITY'].'/'.$id;
        $body = array('status' => false);

        return self::callApi('PUT', $entity, array(), $body);
    }

    public static function help_read(array $conf, $asJson=false, $params='')
    {
        $entity = $conf['ENTITY_READ'];
        if(!$entity) {
            $entity = $conf['ENTITY'];
        }

        return self::callApi('GET', $entity, array(), array(), $params, $asJson);
    }

    public static function help_readById(array $conf, $id)
    {
        $entity = $conf['ENTITY_READ_ID'];
        if(!$entity) {
            $entity = $conf['ENTITY'];
        }

        $data = self::callApi('GET', $entity.'/'.$id);
        return (array) $data['body'];
    }

    public static function help_readEmpty($columns)
    {
        $columns[] = 'id';
        return array_fill_keys($columns, '');
    }

    //--------------------------------------------------------------
    // PRIVATE
    //--------------------------------------------------------------
    private static function callApi($method,
                                    $entity,
                                    array $headers=array(),
                                    array $body=array(),
                                    array $params=array(),
                                    $asJson=false
                                    )
    {
        // Params ---
        $paramsStr = '';
        foreach ($params as $param => $value) {
            $paramsStr .= $param.'='.$value.'&';
        }

        // Url ---
        $url = static::$api_enviroment.$entity.'?'.$paramsStr;

        // Header ---
        $headers_def = [
            'x-auth-token'   => static::$api_auth_token,
            'Accept-Language'=> static::$api_accept_language
        ];
        $headers = array_merge($headers_def, $headers);

        // Call API --------
        // print_r($url); print_r($headers); exit();
        try {
            $response = CallApi::call($method, $url, $headers, $body, $asJson);
        } catch (\Exception $e) {
            static::onError($e->getMessage());
            throw new \Exception($e->getMessage(), 1);
        }

        // Parse result ----
        $ret = self::parseResult($response, $method);

        // Output message errors (!ajax) ---
        if ($ret['result'] == false) {
            static::onError($ret['message']);
        }

        return $ret;
    }

    private static function parseData($data, $columns)
    {
        $dataParsed = array();
        foreach ($columns as $column) {
            if (isset($data[$column])) {
                $dataParsed[$column] = $data[$column];
            }
        }

        return $dataParsed;
    }

    private static function parseResult($result, $method)
    {
        // GET ---
        if ($method == 'GET' && $result->statusCode == '200') {
            return [
                'result' => true,
                'statuscode' => $result->statusCode,
                'body'   => $result->body
            ];
        }

        // POST ---
        if ($method == 'POST' && $result->statusCode == '200') {
            return [
                'result' => true,
                'statuscode' => $result->statusCode,
                'id'     => $result->body->id
            ];
        }

        // PATCH ---
        if ($method == 'PATCH' && $result->statusCode == '202') {
            return [
                'result' => true,
                'statuscode' => $result->statusCode
            ];
        }

        // PUT ---
        if ($method == 'PUT' && ($result->statusCode == '202' || $result->statusCode == '200')) {
            return [
                'result' => true,
                'statuscode' => $result->statusCode
            ];
        }

        // ERROR ---
        $ret = [
            'result' => false,
            'statuscode' => $result->statusCode
        ];

        if (isset($result->body->message)) {
            $ret['message'] = $result->body->message;
        } else {
            $ret['message'] = htmlspecialchars(print_r($result->body, true));
        }
        $ret['message'] = 'Status: '.$result->statusCode.'<br>'.$ret['message'];

        return $ret;
    }

    private static function formatMessage($message)
    {
        return
         '<div style="max-width:500px;color:#333;font-size:12px;background-color:#fff;padding:4px">'.
             $message.
         '</div>';
    }
}
