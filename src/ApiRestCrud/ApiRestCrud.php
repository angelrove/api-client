<?php
/**
 * ApiRestCrud x
 */

namespace angelrove\ApiClient\ApiRestCrud;


use angelrove\ApiClient\ApiRestCrud\ApiRestCrudInterface;
use angelrove\ApiClient\ApiRestCrud\ApiRestCrudHelper;

class ApiRestCrud extends ApiRestCrudHelper implements ApiRestCrudInterface
{
    public static function create($data)
    {
        return parent::help_create(static::CONF, static::$columns, $data);
    }

    public static function update($id, $data)
    {
        return parent::help_update(static::CONF, static::$columns, $id, $data);
    }

    public static function delete($id)
    {
        return parent::help_delete(static::CONF, $id);
    }

    public static function read($asJson=false, array $params=array())
    {
        return parent::help_read(static::CONF, $asJson, $params);
    }

    public static function readById($id)
    {
        return parent::help_readById(static::CONF, $id);
    }

    public static function readEmpty()
    {
        return parent::help_readEmpty(static::$columns);
    }

}
