<?php
/**
 * BasicAuth
 */

namespace angelrove\ApiClient;

use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;

class BasicAuth
{
    //-------------------------------------------------------
    static public function login($user, $pass, $endpoint)
    {
        $client = new Client(['auth' => [$user, $pass]]);
        $request = new Request('GET', $endpoint);

        $response = $client->send($request);

        $content = $response->getBody()->getContents();

        return json_decode($content);
    }
    //-------------------------------------------------------
}